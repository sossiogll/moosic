//
//  ContentView.swift
//  Moosic
//
//  Created by Sossio Giallaurito on 03/03/2020.
//  Copyright © 2020 Sossio Giallaurito. All rights reserved.
//


import SwiftUI
import MessageUI
import CoreMotion

struct ContentView: View {
    
    var conductor = Conductor.sharedInstance
    var songClipIsPlaying = false
    let manager = CMMotionManager()
          var timer: Timer!
         @State var currentCheckInterval = 0
         @State var checkInterval = 30
    
    var body: some View {
        VStack{
            
            Button(action: {self.conductor.playInstrument()
                
            }) {
                
                Text("Play instrument")
                    
                
            }

            Button(action: {self.conductor.prevInstrument()
                
            }) {
                
                Text("Prev")
                    
                
            }
            
            Button(action: {self.conductor.nextInstrument()
                
            }) {
                
                Text("Next")
                    
                
            }
            
        }.onAppear {
            if self.manager.isDeviceMotionAvailable {
                self.manager.deviceMotionUpdateInterval = 1.0 / 60.0
                self.manager.startDeviceMotionUpdates(to: .main) {
                    (data, error) in
                    
                    if (self.currentCheckInterval >= self.checkInterval) {
                        guard let data = data, error == nil else {
                            return
                        }
                        //                print(data.userAcceleration.x)
                        if data.userAcceleration.x > 1.5 {
//                            self.conductor.playKick()
                            self.currentCheckInterval = 0
                        }
                        if data.userAcceleration.x < -1.5 {
//                            self.conductor.playSnare()
                            self.currentCheckInterval = 0
                        }
                        if data.userAcceleration.y > 1 {
                            print("UP", data.userAcceleration.x)
                            self.currentCheckInterval = 0
                        }
                        if data.userAcceleration.y < -1 {
                            print("DOWN", data.userAcceleration.x)
                            self.currentCheckInterval = 0
                        }
                        
                    }
                    
                    self.currentCheckInterval += 1
                }
            }
        }
    }
    }


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}


