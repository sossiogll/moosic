//
//  Conductor.swift
//  MoosicApp
//
//  Created by Sossio Giallaurito on 04/03/2020.
//  Copyright © 2020 Sossio Giallaurito. All rights reserved.
//

import Foundation
import AudioKit


class Conductor{

    static var sharedInstance = Conductor()
    
    private var instruments : [Instrument]?
    private var instrumentMixer : AKMixer?
    private var playerMixer : AKMixer?
    private var generalMixer : AKMixer?
    private var player: AKAudioPlayer?
    private var recorer: Recorder?
    private var currentInstrument : Int
    private var trackList : [Track]

    init(){

        currentInstrument = 0;
        trackList = [Track]()

        initMixers()
        initInstruments()
        initAudioKit()
        

        print("Init conductor")
    }
    
    private func initInstruments(){
        
        instruments = [Instrument]()
        
        for instInfo in instrumentList{
            
            instruments!.append(
                Instrument(
                    instrumentName: instInfo.instrumentName,
                    samplePath: instInfo.getFullPath(),
                    defaultPitch: instInfo.defaultPitch
                )
            )
                    
            
        }
        
        for instrument in instruments!{
            
            self.instrumentMixer = AKMixer(self.instrumentMixer ?? AKMixer(), instrument)
            
        }
        
        print("Struments count: \(instruments?.count)")
        
        
    }
    
    private func initMixers(){
        
        self.instrumentMixer = AKMixer()
        self.playerMixer = AKMixer()

    }
    
    
    private func initAudioKit(){
        
        do {
            if #available(iOS 10.0, *) {
                try AKSettings.setSession(category: .playAndRecord, with: [.defaultToSpeaker, .allowBluetooth, .allowBluetoothA2DP])
            } else {
                // Fallback on earlier versions
            }
        } catch {
            print("Errored setting category.")
        }
        
        // Allow audio to play while the iOS device is muted.
          AKSettings.playbackWhileMuted = true
          
          /// Whether to DefaultToSpeaker when audio input is enabled
          AKSettings.defaultToSpeaker = true
                
        self.generalMixer = AKMixer(self.instrumentMixer, self.playerMixer)
        AudioKit.output = self.generalMixer
        
        try! AudioKit.start()
        
    }
    
    
    func playTrack(trackPos : Int){
        
        trackList[trackPos].playTrack()
        
    }
    
    func stopTrack(trackPos : Int){
        
        trackList[trackPos].stopTrack()
        
    }
    
    func deleteTrack(trackPos : Int){
        
        trackList[trackPos].stopTrack()
        trackList.remove(at: trackPos)
        
    }
    
    func nextInstrument(){
        
        self.currentInstrument = self.currentInstrument + 1
        
        if(self.currentInstrument == instruments!.count){
            self.currentInstrument = 0
        }
        
        print("Current instrument: \(self.currentInstrument)")

        
    }
    
    func prevInstrument(){
        
        self.currentInstrument = self.currentInstrument - 1

        if(self.currentInstrument == -1){
            self.currentInstrument = instruments!.count - 1
        }
        
        print("Current instrument: \(self.currentInstrument)")
        
    }
    
    func playInstrument(){
        
        instruments![currentInstrument].play()
        
    }
    
    
    func playInstrument(pitch: UInt8){
        
        instruments![currentInstrument].play(pitch: pitch)
        
    }
        
    func playInstrument(pitch: UInt8, velocity: UInt8){
        
        instruments![currentInstrument].play(pitch: pitch, velocity: velocity)
        
    }
        
    func playInstrument(pitch: UInt8, velocity: UInt8, channel: UInt8){
        
        instruments![currentInstrument].play(pitch: pitch, velocity: velocity, channel: channel)
        
    }
    

    
    
}
