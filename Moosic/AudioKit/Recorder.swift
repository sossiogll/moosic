//
//  Recorder.swift
//  Moosic
//
//  Created by Sossio Giallaurito on 06/03/2020.
//  Copyright © 2020 Sossio Giallaurito. All rights reserved.
//

import Foundation
import AudioKit

class Recorder{
    
    private var fileUrl : String
    private var player : AKAudioPlayer?
    private var audioFile : AKAudioFile?
    private var recorder : AKNodeRecorder?
    private var source : AKNode
    
    init(fileUrl: String, source: AKNode){
        
        self.source = source
        self.fileUrl = fileUrl
        
        do{
            try self.audioFile = AKAudioFile(readFileName: self.fileUrl)
            try self.player = AKAudioPlayer(file: self.audioFile ?? silent(samples: 128))
            try self.recorder = try? AKNodeRecorder(node: self.source)
        }
        catch{
            
        }
    
        
    }
    
    init(source: AKNode){
        
        self.source = source
        self.fileUrl = ""
        
        do{
            try self.audioFile = silent(samples: 128)
            try self.player = AKAudioPlayer(file: self.audioFile!)
            try self.recorder = try? AKNodeRecorder(node: self.source)
        }
        catch{
            
        }
    
        
    }
    
    
    public func silent(
        samples: Int64,
        baseDir: AKAudioFile.BaseDirectory = .temp,
        name: String = "") throws -> AKAudioFile {

        if samples < 0 {
            AKLog( "ERROR AKAudioFile: cannot create silent AKAUdioFile with negative samples count !")
            throw NSError(domain: NSURLErrorDomain, code: NSURLErrorCannotCreateFile, userInfo:nil)
        } else if samples == 0 {
            let emptyFile = try AKAudioFile(writeIn: baseDir, name: name)
            // we return it as a file for reading
            return try AKAudioFile(forReading: emptyFile.url)
        }

        let zeros = [Float](zeros: Int(samples))
        let silentFile = try AKAudioFile(createFileFromFloats: [zeros, zeros], baseDir: baseDir, name: name)

        return try AKAudioFile(forReading: silentFile.url)
    }
    
    
}
