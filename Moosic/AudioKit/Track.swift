//
//  Track.swift
//  Moosic
//
//  Created by Sossio Giallaurito on 06/03/2020.
//  Copyright © 2020 Sossio Giallaurito. All rights reserved.
//

import Foundation
import AudioKit

class Track{
    
    private var fileUrl : String
    private var player : AKAudioPlayer?
    private var audioFile : AKAudioFile
    
    init(trackUrl : String){
        
        self.fileUrl = trackUrl
        
        do{
            
            try self.audioFile = AKAudioFile(readFileName: trackUrl)
            
        }catch{
            
            fatalError("Unexpected error for track: \(error).")

        }
        
        do{
            try self.player = AKAudioPlayer(file: self.audioFile)
        }
        catch{
            
        }
    }
    
    init(audioFile : AKAudioFile){
        
        self.fileUrl = ""
        self.audioFile = audioFile
        do{
            try self.player = AKAudioPlayer(file: self.audioFile)
        }
        catch{
            
        }
        
    }
    
    func playTrack(){
        
        self.player!.play()
        
    }
    
    func stopTrack(){
        
        self.player!.stop()
        
    }
    
}
