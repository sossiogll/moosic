//
//  InstrumentsList.swift
//  Moosic
//
//  Created by Sossio Giallaurito on 04/03/2020.
//  Copyright © 2020 Sossio Giallaurito. All rights reserved.
//

import Foundation

struct InstrumentInfo{
    
    let instrumentName:String
    let instrumentSamplesDirectory : String
    let instrumentSamplesNames:[String]
    let instrumentSaplesFormats:[String]
    let defaultPitch:UInt8
    let defaultSample:Int = 0
    
    
    func getFullPath() -> String{
        return instrumentSamplesDirectory + "/" + instrumentSamplesNames[defaultSample] + instrumentSaplesFormats[defaultSample]
    }
    
    
    func getFullPath(samplePos : Int) -> String{
        
        if(
            samplePos >= 0 &&
            samplePos < instrumentSamplesNames.count &&
            samplePos < instrumentSaplesFormats.count
        ){
            return instrumentSamplesDirectory + "/" + instrumentSamplesNames[samplePos] + instrumentSaplesFormats[samplePos]
        }
        else{
            return getFullPath()
        }
        
    }
    
    
}

let instrumentList = [

    InstrumentInfo(
        instrumentName: "Sound1",
        instrumentSamplesDirectory: "Sounds/sound1",
        instrumentSamplesNames: ["1","2","3","4","5","6","7"],
        instrumentSaplesFormats: [".wav",".wav",".wav",".wav",".wav",".wav",".wav"],
        defaultPitch: 60
    ),
    InstrumentInfo(
        instrumentName: "Sound2",
        instrumentSamplesDirectory: "Sounds/sound2",
        instrumentSamplesNames: ["1","2","3","4","5","6","7"],
        instrumentSaplesFormats: [".wav",".wav",".wav",".wav",".wav",".wav",".wav"],
        defaultPitch: 60
    ),
    InstrumentInfo(
        instrumentName: "Guitar",
        instrumentSamplesDirectory: "Sounds/guitar",
        instrumentSamplesNames: ["1", "2"],
        instrumentSaplesFormats: [".wav",".wav"],
        defaultPitch: 60
    ),
    InstrumentInfo(
        instrumentName: "Drum2",
        instrumentSamplesDirectory: "Sounds/drum2",
        instrumentSamplesNames: ["CYCdh_ElecK03-Kick02", "CYCdh_ElecK03-OpHat", "CYCdh_ElecK03-Snr01", "CYCdh_ElecK03-Tom03", "CYCdh_ElecK03-Tom05", "CYCdh_ElecK04-Cymbal01", "CYCdh_ElecK05-Clap01", "CYCdh_ElecK05-ClHat03" , "CYCdh_ElecK05-Kick02", "CYCdh_ElecK05-OpHat01", "CYCdh_ElecK05-Snr02", "CyCdh_K3Crash-05", "CyCdh_K3HfHat", "CyCdh_K3Kick-03", "CyCdh_K3SdSt-02", "CyCdh_K3Snr-01.wav", "CyCdh_K3Tom-01",   ],
        instrumentSaplesFormats: [".wav", ".wav", ".wav", ".wav", ".wav", ".wav", ".wav", ".wav", ".wav", ".wav", ".wav", ".wav", ".wav", ".wav", ".wav", ".wav", ".wav", ],
        defaultPitch: 60
    )
    

]
